from django.test import TestCase
from authentication.models import User


class UserModel(TestCase):
    def test_creation(self) -> None:
        email: str = 'az@er.ty'
        password: str = 'azerty'
        first_name: str = 'Azerty'
        user: User = User.objects.create_user(
            email=email, password=password, first_name=first_name
        )
        inserted_user: User = User.objects.first()  # type: ignore
        self.assertEqual(user, inserted_user)
        self.assertEqual(email, inserted_user.email)
        self.assertTrue(inserted_user.check_password(password))
        self.assertEqual(first_name, inserted_user.first_name)
