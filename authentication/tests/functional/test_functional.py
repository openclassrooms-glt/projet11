from selenium import webdriver
from selenium.webdriver.support.expected_conditions import visibility_of, \
    url_to_be
from selenium.webdriver.support.wait import WebDriverWait

from authentication.models import User
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import selenium
from selenium.webdriver import ActionChains
from django.test import Client

client = Client()


class TestWithAuthenticatedUser(StaticLiveServerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(email='test@test.com')
        user.set_password('12345')
        user.save()

    def setUp(self):
        self.browser = webdriver.Chrome('drivers/chromedriver')

    def tearDown(self):
        self.browser.close()

    def test_text_is_displayed(self):
        self.browser.get(self.live_server_url)
        alert = self.browser.find_element_by_class_name('page-section')
        self.assertEqual(
            alert.find_element_by_tag_name('h2').text,
            "Colette et Remy"
        )

    def test_control_bad_link_email(self):
        """
        Alternative process:
        1. click on "change password" button
        2. link email invalid
        """
        # start from account page
        start_url = f"{self.live_server_url}/auth/account/"
        self.browser.get(start_url)
        # scroll down
        self.browser.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);"
        )
        # wait for scrolling
        change_pwd_btn = self.browser.find_element_by_id("change_pwd_link")
        WebDriverWait(
            self.browser,
            timeout=2
        ).until(visibility_of(change_pwd_btn))

        # wait for page loading
        WebDriverWait(
            self.browser,
            timeout=2
        ).until(visibility_of(change_pwd_btn))

        # click on the "change password" button
        change_pwd_btn.click()

        change_pwd_url = f"{self.live_server_url}/users/reset/set-password/"

        email = self.browser.find_element_by_id(
            "id_email")
        actions = ActionChains(self.browser)
        actions.click(email)
        actions.send_keys("test@test.com")

        # submit the form
        pwd_submit = self.browser.find_element_by_id("submitbtn")
        actions.click(pwd_submit)
        # scroll down
        self.browser.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);"
        )
        # wait for scrolling
        legal_link = self.browser.find_element_by_id("legal-footer")
        WebDriverWait(
            self.browser,
            timeout=10
        ).until(visibility_of(legal_link))
        # start chained actions
        actions = ActionChains(self.browser)

        response = client.get(
            f"{self.live_server_url}/users/password_reset/done/")
        self.assertEqual(response.status_code, 200)

        set_password = f"{self.live_server_url}/users/reset/Mw/set-password/"
        self.browser.get(set_password)

        legal_link = self.browser.find_element_by_id("legal-footer")
        WebDriverWait(
            self.browser,
            timeout=2
        ).until(visibility_of(legal_link))
        # start chained actions
        actions = ActionChains(self.browser)
        set_pwd_url = f"{self.live_server_url}/users/reset/Mw/set-password/"
        WebDriverWait(
            self.browser,
            timeout=40
        ).until(url_to_be(set_pwd_url))

        self.assertEqual(
            self.browser.find_element_by_id(
                "invalidLink").text, "le lien pour la réinitialisation du "
                                     "mot de passe est invalide, il est possible"
                                     " qu il a deja été utilisé. Relancer la "
                                     "procedure de reinitialisation du mot de passe.")


