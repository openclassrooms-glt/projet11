from django.contrib.auth.decorators import login_required
from django.urls import path
from . import views

app_name = 'authentication'
urlpatterns = [

    path('login/', views.LoginView.as_view(),
         name='login'),
    path('logout/', login_required(views.LogoutView.as_view()),
         name='logout', ),
    path('signup/', views.SignUpView.as_view(),
         name='signup'),
    path('account/', login_required(views.AccountView.as_view()),
         name='account', ),
    path('activate/<str:uid>/<str:token>', views.Activate.as_view(),
         name='activate', ),
    path('inactive/', login_required(views.InactiveView.as_view()),
         name='inactive', ),
]
