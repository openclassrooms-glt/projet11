from typing import Sequence, Type
from django.contrib import admin
from .models import User
from .forms import AdminForm
from products.models import Substitute


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    form: Type[AdminForm] = AdminForm
    list_display: Sequence[str] = ('email', 'name', 'first_name', 'is_active',
                                   'is_staff',)
    list_filter: Sequence[str] = ('is_active', 'is_staff', 'is_superuser',)
    search_fields: Sequence[str] = ('email', 'name',)
    ordering: Sequence[str] = ('email', 'name', 'first_name',)

    def qte_substitute(self, db_field):
        substitute_sum = Substitute.objects.filter(user_id=db_field.id).count()
        return substitute_sum

    qte_substitute.short_description = "Quantité de Substitus sauvés"
