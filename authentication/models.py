from typing import List
from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.text import format_lazy
from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    email: models.EmailField = models.EmailField(
        'email address', max_length=255, unique=True
    )
    first_name: models.CharField = models.CharField(
        'first name', max_length=255, blank=True
    )
    name: models.CharField = models.CharField(
        'last name', max_length=255, blank=True
    )
    is_active: models.BooleanField = models.BooleanField(
        'active', default=True
    )
    is_staff: models.BooleanField = models.BooleanField(
        'staff', default=False
    )

    objects: UserManager = UserManager()

    USERNAME_FIELD: str = 'email'
    REQUIRED_FIELD: List[str] = []

    class Meta:
        verbose_name: str = 'user'
        verbose_name_plural: str = 'users'

    def get_full_name(self) -> str:
        return format_lazy('{first_name} {name}', first_name=self.first_name,
                           name=self.name).strip()