from selenium import webdriver
from selenium.webdriver.support.expected_conditions import visibility_of, \
    url_to_be
from selenium.webdriver.support.wait import WebDriverWait

from authentication.models import User
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import selenium
from selenium.webdriver import ActionChains
from django.test import Client

client = Client()


class TestWithAnonymousUser(StaticLiveServerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = User.objects.create(email='test@test.com')
        user.set_password('12345')
        user.save()

    def setUp(self):
        self.browser = webdriver.Chrome('drivers/chromedriver')

    def tearDown(self):
        self.browser.close()

    def test_display_product(self):
        """
        Display food page.
        """
        product_id = 1
        self.browser.get(f"{self.live_server_url}/products/{product_id}")
        expected_url = f"{self.live_server_url}/products/{product_id}"
        self.assertEqual(self.browser.current_url, expected_url)
