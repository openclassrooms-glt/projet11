import os
import json
import datetime

from django.test import TestCase
from django.test import Client
from django.urls.base import reverse
from django.utils.datastructures import MultiValueDictKeyError

from products.models import Product, Substitute
from authentication.models import User

client = Client()


class TestDeleteProducts(TestCase):
    fixtures = ['BddTest.json']

    def test_delete_user_not_logged(self):
        """Test if user not logged in redirect to login page"""
        response = client.get('/products/delete/1')
        self.assertEqual(response.status_code, 302)
        self.assertIn('/login/', response.url)

    def test_delete_get(self):
        client.login(username='test@test.com', password="Gregory@44")
        response = client.get('/products/delete/2')
        message = "Vous êtes sur le point de supprimer ce substitut ?".encode()
        self.assertEqual(response.status_code, 200)
        self.assertIn(message, response.content)

    def test_delete_post(self):
        client.login(username='test@test.com', password="Gregory@44")
        self.assertEqual(Substitute.objects.all().count(), 2)
        response = client.post('/products/delete/2')
        self.assertEqual(Substitute.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)
        self.assertIn('/myproducts/', response.url)

    def test_delete_false_id(self):
        client.login(username='test@test.com', password="Gregory@44")
        self.assertEqual(Substitute.objects.all().count(), 2)
        response = client.post('/products/delete/1')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(Substitute.objects.all().count(), 2)


class TestDetailProducts(TestCase):
    fixtures = ['BddTest.json']

    def test_detail_success(self):
        client.login(username='test@test.com', password="Gregory@44")
        response = client.get('/products/1/detail/')
        self.assertEqual(response.status_code, 200)
        response = client.get(reverse('products:detail', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, 200)

    def test_detail_fail(self):
        client.login(username='test@test.com', password="Gregory@44")
        response = client.get('/products/9999/detail/')
        self.assertEqual(response.status_code, 404)
        response = client.get(reverse('products:detail', kwargs={'pk': 9999}))
        self.assertEqual(response.status_code, 404)


class TestMyProducts(TestCase):
    fixtures = ['BddTest.json']

    def test_myproducts_not_logged(self):
        response = client.get(reverse('products:myproducts'))
        self.assertEqual(response.status_code, 302)

    def test_myproducts_show_list(self):
        client.login(username='test@test.com', password="Gregory@44")
        p1 = Product.objects.get(pk=1)
        p2 = Product.objects.get(pk=2)
        u = User.objects.get(pk=3)
        Substitute.objects.create(product_id=p1, substitute_id=p2, user_id=u)
        response = client.get(reverse('products:myproducts'))
        self.assertEqual(response.status_code, 200)
        message = "Il n'y a pas de produits enregistrés".encode()
        self.assertNotIn(message, response.content)

    def test_myproducts_show_empty(self):
        client.login(username='test@test.com', password="Gregory@44")
        client.post('/products/delete/3')
        response1 = client.get(reverse('products:myproducts'))
        self.assertEqual(response1.status_code, 200)
        message = "Il n'y a pas de produits enregistrés".encode()
        self.assertIn(message, response1.content)

    def test_myproducts_pages(self):
        client.login(username='test@test.com', password="Gregory@44")
        response = client.get(reverse('products:myproducts') + '?page=1')
        self.assertEqual(response.status_code, 200)
        response = client.get(reverse('products:myproducts') + '?page=2')
        self.assertEqual(response.status_code, 404)


class TestResult(TestCase):
    fixtures = ['BddTest.json']

    def test_search_good_id_return_something(self):
        """
            Produit 1 : nutrition grade c
            Produit 2 : nutrition grade b
            Produit 2 > c : return Produit 2

        """
        response = client.get('/products/1/result/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 9)

    def test_search_good_id_return_nothing(self):
        """
        Produit 1 : nutrition grade c
        Produit 2 : nutrition grade b
        Produit 1 < b : return nothing
        """
        response = client.get('/products/2/result/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 0)

    def test_search_false_id(self):
        with self.assertRaises(Product.DoesNotExist):
            client.get('/products/501/result/')

    def test_allreadysaved_message(self):
        response = client.get('/products/1/result/')
        self.assertNotIn('message', response.context_data)
        response = client.get('/products/1/result/?allreadysaved')
        self.assertEqual(response.status_code, 200)
        self.assertIn('message', response.context_data)


class TestSave(TestCase):
    fixtures = ['BddTest.json']

    def test_save_post_ok_substitute(self):
        """Test all good:
            user is connected
            product id and produc substitutes are ok
            POST request
            DB modification: new item in Substitute
        """

        client.login(username='test@test.com', password="Gregory@44")
        client.post('/products/delete/2')
        client.post('/products/delete/3')
        client.post('/products/delete/4')
        self.assertEqual(Substitute.objects.all().count(), 0)
        response = client.post(
            '/products/save/',
            {
                'product_id': '1',
                'substitute_id': '3',
                'next': '/'
            }
        )
        self.assertEqual(Substitute.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)

    def test_save_post_ok_allreadyexist(self):
        """Test all good but allready saved.
        must return GET 'allreadysaved'

        previous: test_save_post_ok_substitute
        """
        client.login(username='test@test.com', password="Gregory@44")
        client.post('/products/delete/2')
        client.post('/products/delete/3')
        client.post('/products/delete/4')
        response = client.post(
            '/products/save/',
            {
                'product_id': '1',
                'substitute_id': '3',
                'next': '/'
            }
        )
        response = client.post(
            '/products/save/',
            {
                'product_id': '1',
                'substitute_id': '3',
                'next': '/'
            }
        )
        self.assertEqual(Substitute.objects.all().count(), 1)
        self.assertEqual(response.url, '/?allreadysaved')
        self.assertEqual(response.status_code, 302)

    def test_save_user_not_logged(self):
        """Test POST but user not logged in.
       no DB modification
       redirect to index
       """
        self.assertEqual(Substitute.objects.all().count(), 2)
        response = client.post(
            '/products/save/',
            {
                'product_id': '1',
                'substitute_id': '2',
                'next': '/'
            }
        )
        self.assertEqual(Substitute.objects.all().count(), 2)
        self.assertEqual(response.status_code, 302)

    def test_save_get_redirect(self):
        """Test if request is not POST, redirect to index."""
        client.login(username='test@test.com', password="12345")
        response = client.get('/products/save/')
        self.assertEqual(response.status_code, 302)

    def test_file_backup_substitute(self):

        """Create a backup of products in a json file"""

        user_obj = Substitute.objects.filter(user_id=3).order_by('-id')
        list_product = []
        pathjson = 'products/json/backup_3.json'

        for ls in user_obj:
            dict_product = {'product_id_id': ls.product_id_id,
                            'substitute_id_id': ls.substitute_id_id
                            }
            list_product.append(dict_product)

        with open(pathjson, 'w') as json_file:
            json.dump(list_product, json_file)
        longueur=0
        if os.path.exists(pathjson):
            fs = open(pathjson, 'r')
            texte = fs.read()
            longueur = len(texte)
            fs.close()

        self.assertEqual(longueur, 47 )

    def test_file_restore_substitute(self):

        """restore a backup of products in a json file"""
        pathjson = 'products/json/backup_3.json'
        if os.path.exists(pathjson):
            f = open(pathjson)
            data = json.load(f)

            # import data list product
            Substitute.objects.filter( user_id=3).delete()
            for ls in data:
                Substitute.objects.get_or_create(
                    user_id_id=3,
                    product_id_id=ls['product_id_id'],
                    substitute_id_id=ls['substitute_id_id']
                )
        user_obj = Substitute.objects.filter(user_id=3).order_by('-id')
        list_product = []

        for ls in user_obj:
            dict_product = {'product_id_id': ls.product_id_id,
                            'substitute_id_id': ls.substitute_id_id
                            }
            list_product.append(dict_product)
        # Count list_product
        self.assertEqual(len(list_product), 1)


class TestSearch(TestCase):
    fixtures = ['BddTest.json']

    def test_search_success(self):
        """Search real query: return something"""
        response = client.get('/products/search/?query=produit')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 0)

    def test_search_nothing(self):
        """Search false query: return something"""
        response = client.get('/products/search/?query=stuff')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 0)

    def test_search_failed(self):
        # Test a good page
        response = client.get('/products/search/?query=produit&page=1')
        self.assertEqual(response.status_code, 200)
        # PAge 2 does not exist : 404
        response = client.get('/products/search/?query=produit&page=2')
        self.assertEqual(response.status_code, 404)
        # missing query : 500
        with self.assertRaises(MultiValueDictKeyError):
            response = client.get('/products/search/')
