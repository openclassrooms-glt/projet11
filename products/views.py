from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView, DeleteView, RedirectView

from authentication.models import User
from .models import Product, Substitute
import os.path
import json
import datetime

# Create your views here.
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives


class SearchView(ListView):
    """Return products from DB from query."""
    template_name = 'products/search.html'
    paginate_by = 9

    def get_queryset(self):
        return Product.objects.filter(
            product_name__icontains=self.request.GET['query']). \
            order_by('product_name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.request.GET['query']
        context['target'] = 'products:result'
        return context


class ResultView(ListView):
    """Return substitutes with same category and better/equal nutrigrade."""
    template_name = 'products/result.html'

    def get_queryset(self):
        self.product = Product.objects.get(pk=self.kwargs['product_id'])
        return Product.objects \
                   .filter(category=self.product.category) \
                   .filter(nutrition_grades__lte=self.product.nutrition_grades) \
                   .exclude(id=self.kwargs['product_id']) \
                   .order_by('nutrition_grades')[:9]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'allreadysaved' in self.request.GET:
            context['message'] = 'Déjà enregistré'
        context['query'] = self.product.product_name
        context['product'] = self.product
        context['title'] = self.product
        context['target'] = 'products:detail'
        return context


class DetailProductView(DetailView):
    """Show product details."""
    template_name = 'products/detail.html'
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['query'] = kwargs['object'].product_name
        return context


class MyProductsView(LoginRequiredMixin, ListView):
    """Show saved products."""
    template_name = 'products/myproducts.html'
    paginate_by = 5

    def get_queryset(self):
        return Substitute.objects.filter(
            user_id=self.request.user.id).order_by('-id')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'BackupOk' in self.request.GET:
            context['message'] = 'Backup OK, un email ' \
                                 'vient de vous etre envoyé avec votre suvegarde'
        if 'RecupBackupOk' in self.request.GET:
            context['message'] = 'Recovery OK, récuperation de votre derniere Sauvegarde ' \
                                 'effectuée avec succès'
        context['title'] = 'Mes produits'
        context['target'] = 'products:result'
        context['file_exist'] = False
        pathjson = 'products/json/backup_' + str(self.request.user.id) + '.json'
        if os.path.exists(pathjson):
            context['file_exist'] = True

        return context


class DeleteSubstituteView(LoginRequiredMixin, DeleteView):
    """Ask for delete saved product."""
    model = Substitute
    success_url = reverse_lazy('products:myproducts')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Mes produits'
        return context


@login_required
def ImportSubstituteView(request):
    """Import product/substitute."""
    next_url = request.POST['next']
    if request.method == 'POST':
        pathjson = 'products/json/backup_' + str(request.user.id) + '.json'
        if os.path.exists(pathjson):
            f = open(pathjson)
            data = json.load(f)

            # import data list product
            Substitute.objects.filter(
                user_id=request.user.id).delete()
            for ls in data:
                Substitute.objects.get_or_create(
                    user_id_id=request.user.id,
                    product_id_id=ls['product_id_id'],
                    substitute_id_id=ls['substitute_id_id']
                )
            sdate = str(datetime.date.today())

            Email(request.user.email, 'Récupération de votre derniere sauvegarde',
                  sdate + ' Récuperation de votre derniere sauvegarde '
                          ' sur purbeurre.com  a été effectuée avec succès',
                  pathjson
                  )

    return redirect(next_url + "?RecupBackupOk")


@login_required
def ExportSubstituteView(request):
    """Save product/substitute And send email"""
    next_url = request.POST['next']
    if request.method == 'POST':
        user_obj = Substitute.objects.filter(user_id=request.user.id).order_by(
            '-id')
        list_product = []
        pathjson = 'products/json/backup_' + str(request.user.id) + '.json'

        for ls in user_obj:
            dict_product = {'product_id_id': ls.product_id_id,
                            'substitute_id_id': ls.substitute_id_id
                            }
            list_product.append(dict_product)

        with open(pathjson, 'w') as json_file:
            json.dump(list_product, json_file)
        sdate = str(datetime.date.today())
        Email(request.user.email,'Sauvegarde de la liste de vos produits',
              sdate + ' La liste de vos produits sur purbeurre.com '
                      'ont été sauvegardé avec succès',pathjson

              )

    return redirect(next_url + "?BackupOk")


def Email(to, subject_user, smessage_user, file_user):
    subject, from_email, to = subject_user, 'info@purbeurre.com', to
    text_content = smessage_user
    html_content = '<p>Message Purbeurre</p><p></p>' \
                   '<p><strong>' + smessage_user + '</strong> </p>'
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    if file_user != "":
        msg.attach_file(file_user)
    msg.send()


@login_required
def SaveView(request):
    """Save product/substitute."""
    if request.method == 'POST':
        # get data from POST
        product_id = request.POST['product_id']
        substitute_id = request.POST['substitute_id']
        next_url = request.POST['next']
        # get data from DB
        product_obj = Product.objects.get(pk=product_id)
        substitute_obj = Product.objects.get(pk=substitute_id)
        user_obj = User.objects.get(pk=request.user.id)
        # test if all obj are sets
        if product_obj and substitute_obj and user_obj:
            # create Substitute or return "allreadysaved"
            obj, created = Substitute.objects.get_or_create(
                user_id=user_obj,
                product_id=product_obj,
                substitute_id=substitute_obj
            )
            if created:
                return redirect('products:myproducts')
            else:
                return redirect(next_url + "?allreadysaved")
    return redirect('index')
